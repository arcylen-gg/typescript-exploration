import { HasFormatter } from '../interfaces/HasFormatter.js'
// Classes
export class Invoice implements HasFormatter 
{
    constructor(
        readonly client: string,
        private details: string,
        public amount: number // automatically assigned the value
    ) {}
    format()
    {
        return `${this.client} owes P${this.amount} for ${this.details}`;
    }
}