import { HasFormatter } from '../interfaces/HasFormatter.js';
// Classes
export class Payment implements HasFormatter 
{
    constructor(
        readonly recipient: string,
        private details: string,
        public amount: number // automatically assigned the value
    ) {}
    format()
    {
        return `${this.recipient} owes P${this.amount} from ${this.details}`;
    }
}