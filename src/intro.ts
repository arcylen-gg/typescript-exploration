// Browser dont understand typescript
// Typescript as well will be compiled down to javascript using
// (Babel) to be able to read by the browser

// 1. Benefits of using Typescript
 
// a -- Alternative to Javascript(superset)
// extends the javascript can do but with extra features
console.log(123);

// b -- Allow to uses Strict Types
let testNumber = 123
// testNumber = ' Go!'; // Stricting not to dynamically assign
// different datatypes to the declared types
console.log(testNumber)
// { Typescript: 'StrictTypes', Javascript: 'DynamicTypes'}

// c -- Support modern Javascript Features (let, const, 
//  arrow function and etc)

// d -- Extra features (generics, interfaces, tuples etc)

// 2. COMPILING TYPESCRIPT 
// a -- npm install -g typescript
// b -- tsc [filename].ts -w {watch}

// 3. Type Basics
// a -- Iterating the A.2
// Can change the values but not the types of the variable
// b -- Stricting the data types to the functions

const circ = (diameter: number) => {
    return diameter * Math.PI
}
// console.log(circ('test')) // String is not assignable to
// parameter number
console.log(circ(10))

// 4. Objects & Arrays

let names = ['aigoo', 'cornietta'] // Initially define string of array
names.push('mong-mong')
// names.push(3) // we cannot assing number or add other
// type to string array

let mixed = ['ken', 4, 'barbie', false]
mixed.push(10)
mixed.push('hamtaro')
mixed[0] = true

// OBJECTS
let pet = {
    name: 'Aigoo',
    color: 'White',
    age: 2
}
pet.age = 3
pet.name = 'Cornietta'
//pet.age = 'eight' // Cannot reassign
// cannot as well add another property to the object
// pet.greet = ['meow', 'rawr']

pet = { // it has to be the exactly the same property or method
    name: 'Mong mong',
    color: 'Ginger',
    age: 8
}
// Cleaner and more predictable code

// 5. Explicit Types
let e_name: string
let e_age: number
let e_isLoggedIn: boolean

// e_name = 23
e_age = 23

// array

let e_pets: string[] // initialize the string array but basically this is 
// still undefined

let e_ages: number[] = []

// union types
let e_mixed: (string|number|boolean)[] = []
e_mixed.push('hello')
e_mixed.push(20)
e_mixed.push(false)

// don't need to use the parenthesis if it's not array
let uid: string|number

// objects
let e_petObj: object
e_petObj = {
    name: 'Aigoo'
}

let e_petObj2: {
    name: string,
    age: number,
    isACat: boolean
}
e_petObj2 = { name: 'Chukoy', age: 5, isACat: false }

// 6. Dynamic (any) Types
let f_age: any = 25
f_age = 'twenty five'

let f_mixed: any[] = []
f_mixed.push('hello')
f_mixed.push(20)
f_mixed.push(false) 

// 7. Better workflow & tsconfig
// 1 -- tsc --init
    // outDir - will generate the compiled javascript
    // rootDir - will get the ts to be compiled
// 2 -- tsc -w

// 8. Function Basics

let greet: Function
greet = () => {
    console.log('Hello')
}
// Optional parameters use `?`
const add = (a: number, b: number, c?: number | string): void => {
    console.log(a + b)
}
add(5, 10)

const minus = (a: number, b: number): number => {
    return a - b
}
let result = minus(12, 4)

// explicitly declare the return type of the function

// 9 Type Aliases

type StringOrNum = string | number
type objWithNum = {name: string, uid: StringOrNum}

const logDetails = (uid: StringOrNum, item: string) => {
    console.log(`${item} has a uid of ${uid}`)
}

const j_greet = (user: objWithNum) => {
    console.log(`${user.name} says hello`)
}

// 10 Function Signatures

let k_greet: (a: string, b:string) => void
k_greet = (name: string, greeting: string) => {
    console.log(`${name} says ${greeting}`)
}

// signature // Like interface?
let calc: (a: number, b: number, c: string) => number

// must match the signature with the function
calc = (n1: number, n2: number, action: string) => {
    if (action === 'add') {
        return n1 + n2
    } else {
        return n1 - n2
    }
}

// 11 DOM & Type Casting
const aTag = document.querySelector('a')!

if(aTag) {
    console.log(aTag.href)
}
// instead of yung this if condition use `!` 
// `!` i know this exist

console.log(aTag.href)

const form = document.querySelector('form')!
// Element - class
const formtest = document.querySelector('.new-item-form') as HTMLFormElement
// use type casting `as HTMLFormElement`

// inputs
const typeTest = document.querySelector('#type') as HTMLSelectElement
const tofromTest = document.querySelector('#tofrom') as HTMLInputElement
const detailsTest = document.querySelector('#details') as HTMLInputElement
const amountTest = document.querySelector('#amount') as HTMLInputElement

form.addEventListener('submit', (e: Event) => {
    e.preventDefault() // preventing the page refreshing upon submit

    console.log(typeTest.value)
})

// 13 Access Modifiers
// 1. -- Public // default
// 2. -- Private
// 3. -- readonly // can change value inside the class
// but can't redeclare value outside the class

// 14. Modules
// draw backs would be the multiple request in JS
// You can use WEBPACK so it can load all the JS code into
// single file

// 15. Interface
// interfaces
interface IsPerson {
    name: string;
    age: number;
    speak(a: string): void;
    spend(a: number): number;
}

const me: IsPerson = {
    name: 'Arcy',
    age: 24,
    speak(greet: string): void {
        console.log(greet);
    },
    spend(amount: number): number {
        console.log('I spent', amount);
        return amount;
    }
}

const greetPerson = (person: IsPerson) => {
    console.log('hello ', person.name);
}
console.log(me)

// interface with classes


// 18 Generics
// captures the property of the parameter/arguments using `<T>`
const addUID = <T extends {name: string, age: number}>(obj: T) => {
    let uid = Math.floor(Math.random() * 100);
    return {...obj, uid};
}
let docOne = addUID({name: 'Aigoo', age: 40})

interface Resource<T> {
    uid: number;
    resourceName: string;
    data: T;
}

const docTwo: Resource<object> = {
    uid: 1,
    resourceName: 'pets',
    data: {name : 'Aigoo'}
}

const docThree: Resource<string[]> = {
    uid: 1,
    resourceName: 'pets',
    data: ['Aigoo', 'Cornietta']
}

// 19: Enums

enum ResourceType { BOOK, AUTHOR, FILM, DIRECTOR, PERSON }

interface ResourceEnums<T> {
    uid: number;
    resourceType: number;
    data: T;
}

const docEnum1: ResourceEnums<object> = {
    uid: 1,
    resourceType: ResourceType.BOOK, // return the index or the resource Type -- 0
    data: {title : 'name of the wind'}
}

const docEnum2: ResourceEnums<object> = {
    uid: 1,
    resourceType: ResourceType.PERSON, // return the index or the resource Type -- 4
    data: { name: 'yoshi' }
}

// Tuples
// like array
// will define the type of the array per position

let arr = ['aigoo', 2, true];
arr[0] = false;
arr[1] = 'cornietta';

let tup: [string, number, boolean];
// tup = [9, 'test', true];
tup = ['hamtaro', 8, true];