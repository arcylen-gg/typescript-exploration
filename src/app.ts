
import { Invoice } from './classes/Invoice.js';
import { Payment } from './classes/Payment.js';
import { ListTemplate } from './classes/ListTemplate.js';
import { HasFormatter } from './interfaces/HasFormatter.js';

class App {
    element: Document;
    docs: HasFormatter[];
    ul: any;
    list: any;
    constructor(element: Document) 
    {
        this.element = element;
        this.docs = [];
        this.ul = this.element.querySelector('ul')!;
        this.list = new ListTemplate(this.ul);
    }
    onLoad() 
    {
        this.listenForm();
    }
    listenForm()
    {
        const form = this.element.querySelector('.new-item-form') as HTMLFormElement;
        form.addEventListener('submit', this.submitForm.bind(this));
    }           
    submitForm(event: Event) 
    {
        const type = this.element.querySelector('#type') as HTMLSelectElement;
        const tofrom = this.element.querySelector('#tofrom') as HTMLInputElement;
        const details = this.element.querySelector('#details') as HTMLInputElement;
        const amount = this.element.querySelector('#amount') as HTMLInputElement;

        event.preventDefault() // preventing the page refreshing upon submit

        // ... Tuples
        let values: [string, string, number];
        values = [tofrom.value, details.value, amount.valueAsNumber];

        let doc: HasFormatter;
        if (type.value === 'invoice') {
            doc = new Invoice(...values)
            this.docs.push(doc)
        } else {
            doc = new Payment(...values)
            this.docs.push(doc)
        }
        this.list.render(doc, type.value, 'end');
    }
}
const app = new App(document);
app.onLoad();