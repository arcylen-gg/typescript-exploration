import { Invoice } from './classes/Invoice.js';
import { Payment } from './classes/Payment.js';
import { ListTemplate } from './classes/ListTemplate.js';
class App {
    constructor(element) {
        this.element = element;
        this.docs = [];
        this.ul = this.element.querySelector('ul');
        this.list = new ListTemplate(this.ul);
    }
    onLoad() {
        this.listenForm();
    }
    listenForm() {
        const form = this.element.querySelector('.new-item-form');
        form.addEventListener('submit', this.submitForm.bind(this));
    }
    submitForm(event) {
        const type = this.element.querySelector('#type');
        const tofrom = this.element.querySelector('#tofrom');
        const details = this.element.querySelector('#details');
        const amount = this.element.querySelector('#amount');
        event.preventDefault(); // preventing the page refreshing upon submit
        // ... Tuples
        let values;
        values = [tofrom.value, details.value, amount.valueAsNumber];
        let doc;
        if (type.value === 'invoice') {
            doc = new Invoice(...values);
            this.docs.push(doc);
        }
        else {
            doc = new Payment(...values);
            this.docs.push(doc);
        }
        this.list.render(doc, type.value, 'end');
    }
}
const app = new App(document);
app.onLoad();
