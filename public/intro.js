"use strict";
// Browser dont understand typescript
// Typescript as well will be compiled down to javascript using
// (Babel) to be able to read by the browser
// 1. Benefits of using Typescript
// a -- Alternative to Javascript(superset)
// extends the javascript can do but with extra features
console.log(123);
// b -- Allow to uses Strict Types
let testNumber = 123;
// testNumber = ' Go!'; // Stricting not to dynamically assign
// different datatypes to the declared types
console.log(testNumber);
// { Typescript: 'StrictTypes', Javascript: 'DynamicTypes'}
// c -- Support modern Javascript Features (let, const, 
//  arrow function and etc)
// d -- Extra features (generics, interfaces, tuples etc)
// 2. COMPILING TYPESCRIPT 
// a -- npm install -g typescript
// b -- tsc [filename].ts -w {watch}
// 3. Type Basics
// a -- Iterating the A.2
// Can change the values but not the types of the variable
// b -- Stricting the data types to the functions
const circ = (diameter) => {
    return diameter * Math.PI;
};
// console.log(circ('test')) // String is not assignable to
// parameter number
console.log(circ(10));
// 4. Objects & Arrays
let names = ['aigoo', 'cornietta']; // Initially define string of array
names.push('mong-mong');
// names.push(3) // we cannot assing number or add other
// type to string array
let mixed = ['ken', 4, 'barbie', false];
mixed.push(10);
mixed.push('hamtaro');
mixed[0] = true;
// OBJECTS
let pet = {
    name: 'Aigoo',
    color: 'White',
    age: 2
};
pet.age = 3;
pet.name = 'Cornietta';
//pet.age = 'eight' // Cannot reassign
// cannot as well add another property to the object
// pet.greet = ['meow', 'rawr']
pet = {
    name: 'Mong mong',
    color: 'Ginger',
    age: 8
};
// Cleaner and more predictable code
// 5. Explicit Types
let e_name;
let e_age;
let e_isLoggedIn;
// e_name = 23
e_age = 23;
// array
let e_pets; // initialize the string array but basically this is 
// still undefined
let e_ages = [];
// union types
let e_mixed = [];
e_mixed.push('hello');
e_mixed.push(20);
e_mixed.push(false);
// don't need to use the parenthesis if it's not array
let uid;
// objects
let e_petObj;
e_petObj = {
    name: 'Aigoo'
};
let e_petObj2;
e_petObj2 = { name: 'Chukoy', age: 5, isACat: false };
// 6. Dynamic (any) Types
let f_age = 25;
f_age = 'twenty five';
let f_mixed = [];
f_mixed.push('hello');
f_mixed.push(20);
f_mixed.push(false);
// 7. Better workflow & tsconfig
// 1 -- tsc --init
// outDir - will generate the compiled javascript
// rootDir - will get the ts to be compiled
// 2 -- tsc -w
// 8. Function Basics
let greet;
greet = () => {
    console.log('Hello');
};
// Optional parameters use `?`
const add = (a, b, c) => {
    console.log(a + b);
};
add(5, 10);
const minus = (a, b) => {
    return a - b;
};
let result = minus(12, 4);
const logDetails = (uid, item) => {
    console.log(`${item} has a uid of ${uid}`);
};
const j_greet = (user) => {
    console.log(`${user.name} says hello`);
};
// 10 Function Signatures
let k_greet;
k_greet = (name, greeting) => {
    console.log(`${name} says ${greeting}`);
};
// signature // Like interface?
let calc;
// must match the signature with the function
calc = (n1, n2, action) => {
    if (action === 'add') {
        return n1 + n2;
    }
    else {
        return n1 - n2;
    }
};
// 11 DOM & Type Casting
const aTag = document.querySelector('a');
if (aTag) {
    console.log(aTag.href);
}
// instead of yung this if condition use `!` 
// `!` i know this exist
console.log(aTag.href);
const form = document.querySelector('form');
// Element - class
const formtest = document.querySelector('.new-item-form');
// use type casting `as HTMLFormElement`
// inputs
const typeTest = document.querySelector('#type');
const tofromTest = document.querySelector('#tofrom');
const detailsTest = document.querySelector('#details');
const amountTest = document.querySelector('#amount');
form.addEventListener('submit', (e) => {
    e.preventDefault(); // preventing the page refreshing upon submit
    console.log(typeTest.value);
});
const me = {
    name: 'Arcy',
    age: 24,
    speak(greet) {
        console.log(greet);
    },
    spend(amount) {
        console.log('I spent', amount);
        return amount;
    }
};
const greetPerson = (person) => {
    console.log('hello ', person.name);
};
console.log(me);
// interface with classes
// 18 Generics
// captures the property of the parameter/arguments using `<T>`
const addUID = (obj) => {
    let uid = Math.floor(Math.random() * 100);
    return Object.assign(Object.assign({}, obj), { uid });
};
let docOne = addUID({ name: 'Aigoo', age: 40 });
const docTwo = {
    uid: 1,
    resourceName: 'pets',
    data: { name: 'Aigoo' }
};
const docThree = {
    uid: 1,
    resourceName: 'pets',
    data: ['Aigoo', 'Cornietta']
};
// 19: Enums
var ResourceType;
(function (ResourceType) {
    ResourceType[ResourceType["BOOK"] = 0] = "BOOK";
    ResourceType[ResourceType["AUTHOR"] = 1] = "AUTHOR";
    ResourceType[ResourceType["FILM"] = 2] = "FILM";
    ResourceType[ResourceType["DIRECTOR"] = 3] = "DIRECTOR";
    ResourceType[ResourceType["PERSON"] = 4] = "PERSON";
})(ResourceType || (ResourceType = {}));
const docEnum1 = {
    uid: 1,
    resourceType: ResourceType.BOOK,
    data: { title: 'name of the wind' }
};
const docEnum2 = {
    uid: 1,
    resourceType: ResourceType.PERSON,
    data: { name: 'yoshi' }
};
// Tuples
// like array
// will define the type of the array per position
let arr = ['aigoo', 2, true];
arr[0] = false;
arr[1] = 'cornietta';
let tup;
// tup = [9, 'test', true];
tup = ['hamtaro', 8, true];
