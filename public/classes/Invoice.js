// Classes
export class Invoice {
    constructor(client, details, amount // automatically assigned the value
    ) {
        this.client = client;
        this.details = details;
        this.amount = amount;
    }
    format() {
        return `${this.client} owes P${this.amount} for ${this.details}`;
    }
}
