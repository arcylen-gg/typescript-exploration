// Classes
export class Payment {
    constructor(recipient, details, amount // automatically assigned the value
    ) {
        this.recipient = recipient;
        this.details = details;
        this.amount = amount;
    }
    format() {
        return `${this.recipient} owes P${this.amount} from ${this.details}`;
    }
}
